from flask import Flask, jsonify, abort, make_response, request, url_for


application = Flask(__name__)

users = [
    {
        'id': 1,
        'name': u'John',
        'surname': u'Doe',
        'position': u'Manager'
    },
    {
        'id': 2,
        'name': u'Jack',
        'surname': u'Daniels',
        'position': u'Dev'
    },

]
user_attrs = ('name', 'surname', 'position',)

@application.route('/api/v1/users', methods=['GET'])
def get_users():

    return jsonify({ 'users': [make_public_user(user) for user in users]})


@application.route('/api/v1/users/<int:user_id>', methods=['GET'])
def get_user(user_id):

    user = [user for user in users if user['id'] == user_id]
    if len(user) == 0:
        abort(404)

    return jsonify({'user': user[0]})


@application.route('/api/v1/users', methods=['POST'])
def create_user():

    if not request.json:
        abort(400)

    for attr in user_attrs:
        if attr not in request.json:
            abort(400)

    if len(users) == 0:
        user_id = 1
    else:
        user_id = users[-1]['id'] + 1

    user = {
        'id': user_id,
        'name': request.json['name'],
        'surname': request.json.get('surname', ""),
        'position': request.json.get('position', "")
    }

    users.append(user)

    return jsonify({'user': user}), 201

@application.route('/api/v1/users/<int:user_id>', methods=['DELETE'])
def delete_user(user_id):

    user = [user for user in users if user['id'] == user_id]
    if len(user) == 0:
        abort(404)

    users.remove(user[0])

    return jsonify({'result': True})

def make_public_user(user):

    new_user = {}
    for field in user:
        if field == 'id':
            new_user['uri'] = url_for('get_user', user_id=user['id'], _external=True)
        else:
            new_user[field] = user[field]

    return new_user


@application.errorhandler(400)
def bad_request(error):

    return make_response(jsonify({'error': 'Bad request'}), 400)


@application.errorhandler(404)
def not_found(error):

    return make_response(jsonify({'error': 'Not found'}), 404)


if __name__ == "__main__":
    application.run(host="0.0.0.0", port=8000,debug=True)
