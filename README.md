# README #

This boilerplate is using [Vagrant](https://www.vagrantup.com) to provison test Docker environment with simple Rest API demo on virtual machine.

### Requirements ###

The only software requirement is **vagrant** v1.7.0 and above. You can download the latest from [Vagrant download page](https://www.vagrantup.com/downloads.html).


Also you need a *vagrant provider* by your choice, you can list available providers in [documentation](https://www.vagrantup.com/docs/providers/). This boilerplate was tested with [Virtualbox](https://www.vagrantup.com/docs/virtualbox/).


### Run environment ###

`vagrant up`

- will start a virtual machine (vm) based on your provider with the latest [Ubuntu Xenial image](https://atlas.hashicorp.com/ubuntu/boxes/xenial64)
- will provision the environment via Ansible playbook
- provisioning will start 2 Docker containers, 1 with *nginx* as proxy web server, second one with *the Rest API python application* built with [Flask python microframwork](http://flask.pocoo.org)


`vagrant provision`

- the command will run vagrant provisioner(s) again

`vagrant halt`

- will stop vm ( and environment as well )

`vagrant destroy`

- will destroy vm and clean up

### Test Rest API ###

As there is a port-forwarding set between vm and your local machine (80 -> 8000), you can test Rest API easily without ssh login to vagrant box.

#### List all users ####

`$ curl -i http://localhost:8000/api/v1/users`

```
HTTP/1.1 200 OK
Server: nginx/1.13.1
Date: Sun, 25 Jun 2017 16:25:37 GMT
Content-Type: application/json
Content-Length: 307
Connection: close

{
  "users": [
    {
      "name": "John",
      "position": "Manager",
      "surname": "Doe",
      "uri": "http://localhost:8000/api/v1/users/1"
    },
    {
      "name": "Jack",
      "position": "Dev",
      "surname": "Daniels",
      "uri": "http://localhost:8000/api/v1/users/2"
    }
  ]
}
```

#### Get user ####

`$ curl -i http://localhost:8000/api/v1/users/1`

```
HTTP/1.1 200 OK
Server: nginx/1.13.1
Date: Sun, 25 Jun 2017 16:27:48 GMT
Content-Type: application/json
Content-Length: 104
Connection: close

{
  "user": {
    "id": 1,
    "name": "John",
    "position": "Manager",
    "surname": "Doe"
  }
}
```

#### Create user ####

`$ curl -i -H "Content-Type: application/json" -X POST -d '{"name":"Jim","surname":"Beam", "position": "director"}' http://localhost:8000/api/v1/users`

```
HTTP/1.1 201 CREATED
Server: nginx/1.13.1
Date: Sun, 25 Jun 2017 16:31:16 GMT
Content-Type: application/json
Content-Length: 105
Connection: close

{
  "user": {
    "id": 3,
    "name": "Jim",
    "position": "director",
    "surname": "Beam"
  }
}
```

#### Delete user ####

`$ curl -i -X DELETE http://localhost:8000/api/v1/users/3`

```
HTTP/1.1 200 OK
Server: nginx/1.13.1
Date: Sun, 25 Jun 2017 16:35:33 GMT
Content-Type: application/json
Content-Length: 21
Connection: close

{
  "result": true
}
```
